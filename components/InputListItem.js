import React,{useState} from "react";
import { StyleSheet,View,TextInput,Button} from "react-native";

const InputListItem=props=>{
    const [enteredlistdata,setEnteredListData]=useState('');

    const getListData=(inputlistData)=>{
        setEnteredListData(inputlistData);
      }
      const reSetFun=()=>{
        props.onAdd(enteredlistdata);
        setEnteredListData ('') ;
      }
    return (
        <View style={styles.inputArea}>
            <TextInput placeholder="Enter the item here" style={styles.inputTextStyle} onChangeText={getListData} value={enteredlistdata}/>
            <Button title="ADD" color='#7373a5' onPress={reSetFun}/>
        </View>
    )
}

const styles=StyleSheet.create({
    inputArea:{
        width:"95%",
        flexDirection:"row",
        justifyContent:'space-around',
        marginHorizontal:3,
        marginVertical:50
      },
      inputTextStyle:{
        borderColor:'#f5deb3',
        borderWidth:1,
        width:230,
        height:40,
        paddingLeft:10,
        color:'#003d66',
        backgroundColor:'white',
        fontSize:16
      },
     
});

export default InputListItem;