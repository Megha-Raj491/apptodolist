import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';


const MainButton = props => {
  return (
    <TouchableOpacity>
      <View>
        <Text style={styles.buttonText}>{props.children}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  buttonText: {
    color: 'white',
    fontSize: 18
  }
});

export default MainButton;
