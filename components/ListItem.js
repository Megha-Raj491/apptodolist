import React, { useState } from "react";
import { StyleSheet,View,Text, Modal,TextInput,Button,Alert} from "react-native";
import MainButton from "./MainButton";
import {AntDesign} from '@expo/vector-icons';



const ListItem=props=>{
    const [isVisible,setIsVisible]=useState(false);
    const [currentValue,setCurrentValue]=useState(props.title);
    const [editText,setEditText]=useState(currentValue);
    

    const getEditText=(newText)=>{
        setEditText(newText);
    }
    const editTextHandler=()=>{
      if(!editText.trim()){
        Alert.alert("Don't make empty")
        return;
      }
        setCurrentValue(editText);
        setIsVisible(false);
    }
    const toBack=()=>{
      setIsVisible(false);
    }
   
   
    return (
    <View style={styles.iconWithStyle}>
        <Modal visible={isVisible}>
           <View><Button title="Back" onPress={toBack}/></View>
            <View style={styles.editBox}>
            <TextInput style={styles.editInput} onChangeText={getEditText} value={editText}/>
            <Button title="Save" onPress={editTextHandler}/>  
           </View>
        </Modal>
        <Text style={styles.listStyle}>{currentValue}</Text>
        <MainButton>
          <AntDesign name="edit" size={28} color="white" onPress={()=>setIsVisible(true)}/>
        </MainButton>
        <MainButton>
          <AntDesign name="delete" size={28} color="white" onPress={props.onDelete.bind(this,props.id)}/>
        </MainButton>
    </View>)
}

const styles=StyleSheet.create({
    listStyle:{
        backgroundColor:'#1d1d2f',
        color:'#99d6ff',
        marginTop:10,
        height:50,
        padding:10,
        paddingTop:15,
        fontSize:18,
        width:230,
        marginHorizontal:10,
        

      },
      iconWithStyle:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-evenly',
        width:"95%",
    },
    editBox:{
        flexDirection:'row',
        justifyContent:'space-around',
        padding:50,
        marginTop:100,
        
    },
    editInput:{
        borderBottomColor:'black',
        width:200,
        borderBottomWidth:1,
       
    },
    

      

});

export default ListItem;