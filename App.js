import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text,View ,FlatList, Alert,} from 'react-native';

import InputListItem from './components/InputListItem';
import ListItem from './components/ListItem';

export default function App() {

  const [listData,setListData]=useState([]);
  
  const addDataToList=(listName)=>{
    if(!listName.trim()){
      Alert.alert("Please enter a item")
      return;
    }
    else{
    setListData(currentInput=>[...currentInput,{id:Math.random().toString(),value:listName}]);
    }
    
  }

  const deleteHandler=(listId)=>{
      setListData(currentList=>{
        return listData.filter((list)=>list.id!==listId)})
  }
 
  return (
    <View style={styles.container}>
      <View style={styles.headerStyle}>
        <Text style={styles.headerTextStyle} >ToDo LIST</Text>
      </View>
      <InputListItem onAdd={addDataToList}/>
      <FlatList 
      keyExtractor={(item,index)=>item.id}
      data={listData}
      renderItem={itemData=><ListItem visible={false} id={itemData.item.id} onDelete={deleteHandler} title={itemData.item.value}/> }     
     />
      
      <StatusBar style="auto" backgroundColor="#676798"/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#33334d',
  },
  headerStyle:{
    alignItems:"center",
    justifyContent:'center',
    backgroundColor:'#c2c2d6',
    width:'100%',
    height:100
    
  },
  headerTextStyle:{
    color:'#008080',
    fontSize:28,
    fontWeight:'bold',
    letterSpacing:20,
    paddingTop:35,
    fontFamily:'monospace'
  },
  
  listItemStyle:{
    width:"80%",
    marginHorizontal:50,
  },
});
